import React from 'react';
import logo from '../../assets/logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>
          Organización de componentes por dominio en un proyecto React.
        </h2>
        <p style={{ width: '80%'}}>
          La idea principal de esta organización es agrupar todos los elementos que definen y hacen posible el funcionamiento de un Componente dentro de una misma carpeta, es decir, todos a un mismo nivel. Incluyendo también todos aquellos sub-componentes que formen parte del Componente principal.<br /><br />
          La definición de cada "dominio" depende de la naturaleza del proyecto, en algunos casos puede ser basado en el "router", en otros casos puede ser basado en los elementos del menú, y en otros casos puede ser basado en las definiciones del mismo modelo de negocio.<br />
        </p>

        <h2>Links de interés</h2>
        <ul>
          <li>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Aprender React
        </a>

          </li>
          <li>
          <a
          className="App-link"
          href="https://medium.com/@hassan.djirdeh/domain-driven-react-redux-a474ecf7d126"
          target="_blank"
          rel="noopener noreferrer"
        >
          Domain-Driven File Structuring -React/Redux
        </a>
          </li>
          <li>
          <a
          className="App-link"
          href="https://reactjs.org/docs/faq-structure.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          File Structure (React FAQs)
        </a>
          </li>
          <li>
          <a
          className="App-link"
          href="https://medium.com/quick-code/how-to-integrate-eslint-prettier-in-react-6efbd206d5c4"
          target="_blank"
          rel="noopener noreferrer"
        >
          Eslint + Prettier
        </a>
          </li>
          <li>
          <a
          className="App-link"
          href="https://github.com/airbnb/javascript"
          target="_blank"
          rel="noopener noreferrer"
        >
          Eslint: Airbnb javascript
        </a>
          </li>
        </ul>
      </header>
    </div>
  );
}

export default App;
