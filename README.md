Proyecto creado con "create-react-app".

## INFO

Ejemplo de la organización por dominio de un proyecto React.

## Configuración

Después de la creación del proyecto se hizo un "npm run eject" el cual no es obligatorio para utilizar la estructura de dominio, solo es una buena práctica para proyectos que se quieran mantener más ligeros o en los cuales sea necesario hacer modificaciones en el webpack.
